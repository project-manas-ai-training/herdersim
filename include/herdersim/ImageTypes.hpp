//
// Created by reuben on 16/1/17.
//

#ifndef HERDERSIM_IMAGETYPES_HPP
#define HERDERSIM_IMAGETYPES_HPP


enum ImageTypes {
	DOG,
	SHEEP,
	SKUNK
};


#endif //HERDERSIM_IMAGETYPES_HPP
